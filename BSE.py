# -*- coding: utf-8 -*-
#
# BSE: The Bristol Stock Exchange
#
# Version 1.2; November 17th, 2012. 
#
# Copyright (c) 2012, Dave Cliff
#
#
# ------------------------
#
# MIT Open-Source License:
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# ------------------------
#
#
#
# BSE is a very simple simulation of automated execution traders
# operating on a very simple model of a limit order book (LOB) exchange
#
# major simplifications in this version:
#       (a) only one financial instrument being traded
#       (b) traders can only trade contracts of size 1 (will add variable quantities later)
#       (c) each trader can have max of one order per single orderbook.
#       (d) traders can replace/overwrite earlier orders, but cannot cancel
#       (d) simply processes each order in sequence and republishes LOB to all traders
#           => no issues with exchange processing latency/delays or simultaneously issued orders.
#
# NB this code has been written to be readable, not efficient!



# could import pylab here for graphing etc
import sys
import math
import random
import numpy as np
import scipy
from scipy.stats import expon
from scipy.stats import norm
from scipy.interpolate import interp1d


bse_sys_minprice = 1  # minimum price in the system, in cents/pennies
bse_sys_maxprice = 1000  # maximum price in the system, in cents/pennies
ticksize = 1  # minimum change in price, in cents/pennies



# an Order has a trader id, a type (buy/sell) price, quantity, and time it was issued
class Order:

        def __init__(self, tid, otype, price, qty, time):
                self.tid = tid
                self.otype = otype
                self.price = price
                self.qty = qty
                self.time = time

        def __str__(self):
                return '[%s %s P=%03d Q=%s T=%5.2f]' % (self.tid, self.otype, self.price, self.qty, self.time)



# Orderbook_half is one side of the book: a list of bids or a list of asks, each sorted best-first

class Orderbook_half:

        def __init__(self, booktype, worstprice):
                # booktype: bids or asks?
                self.booktype = booktype
                # dictionary of orders received, indexed by Trader ID
                self.orders = {}
                # limit order book, dictionary indexed by price, with order info
                self.lob = {}
                # anonymized LOB, lists, with only price/qty info
                self.lob_anon = []
                # summary stats
                self.best_price = None
                self.best_tid = None
                self.worstprice = worstprice
                self.n_orders = 0  # how many orders?
                self.lob_depth = 0  # how many different prices on lob?


        def anonymize_lob(self):
                # anonymize a lob, strip out order details, format as a sorted list
                # NB for asks, the sorting should be reversed
                self.lob_anon = []
                for price in sorted(self.lob):
                        qty = self.lob[price][0]
                        self.lob_anon.append([price, qty])


        def build_lob(self):
                # take a list of orders and build a limit-order-book (lob) from it
                # NB the exchange needs to know arrival times and trader-id associated with each order
                # returns lob as a dictionary (i.e., unsorted)
                # also builds anonymized version (just price/quantity, sorted, as a list) for publishing to traders
                self.lob = {}
                for tid in self.orders:
                        order = self.orders.get(tid)
                        price = order.price
                        if price in self.lob:
                                # update existing entry
                                qty = self.lob[price][0]
                                orderlist = self.lob[price][1]
                                orderlist.append([order.time, order.qty, order.tid])
                                self.lob[price] = [qty + order.qty, orderlist]
                        else:
                                # create a new dictionary entry
                                self.lob[price] = [order.qty, [[order.time, order.qty, order.tid]]]
                # create anonymized version
                self.anonymize_lob()
                # record best price and associated trader-id
                if len(self.lob) > 0 :
                        if self.booktype == 'Bid':
                                self.best_price = self.lob_anon[-1][0]
                        else :
                                self.best_price = self.lob_anon[0][0]
                        self.best_tid = self.lob[self.best_price][1][0][2]
                else :
                        self.best_price = None
                        self.best_tid = None


        def book_add(self, order):
                # add order to the dictionary holding the list of orders
                # either overwrites old order from this trader
                # or dynamically creates new entry in the dictionary
                # so, max of one order per trader per list
                self.orders[order.tid] = order
                self.n_orders = len(self.orders)
                self.build_lob()


        def book_del(self, order):
                # delete order to the dictionary holding the orders
                # assumes max of one order per trader per list
                # checks that the Trader ID does actually exist in the dict before deletion
                if self.orders.get(order.tid) != None :
                        del(self.orders[order.tid])
                        self.n_orders = len(self.orders)
                        self.build_lob()


        def delete_best(self):
                # delete order: when the best bid/ask has been hit, delete it from the book
                # the TraderID of the deleted order is return-value, as counterparty to the trade
                best_price_orders = self.lob[self.best_price]
                best_price_qty = best_price_orders[0]
                best_price_counterparty = best_price_orders[1][0][2]
                if best_price_qty == 1:
                        # here the order deletes the best price
                        del(self.lob[self.best_price])
                        del(self.orders[best_price_counterparty])
                        self.n_orders = self.n_orders - 1
                        if self.n_orders > 0:
                                self.best_price = min(self.lob.keys())
                                self.lob_depth = len(self.lob.keys())
                        else:
                                self.best_price = self.worstprice
                                self.lob_depth = 0
                else:
                        # best_bid_qty>1 so the order decrements the quantity of the best bid
                        # update the lob with the decremented order data
                        self.lob[self.best_price] = [best_price_qty - 1, best_price_orders[1][1:]]

                        # update the bid list: counterparty's bid has been deleted
                        del(self.orders[best_price_counterparty])
                        self.n_orders = self.n_orders - 1
                self.build_lob()
                return best_price_counterparty



# Orderbook for a single instrument: list of bids and list of asks

class Orderbook(Orderbook_half):

        def __init__(self):
                self.bids = Orderbook_half('Bid', bse_sys_minprice)
                self.asks = Orderbook_half('Ask', bse_sys_maxprice)
                self.tape = []



# Exchange's internal orderbook

class Exchange(Orderbook):

        def add_order(self, order):
                # add an order to the exchange and update all internal records
                tid = order.tid
                if order.otype == 'Bid':
                        self.bids.book_add(order)
                        best_price = self.bids.lob_anon[-1][0]
                        self.bids.best_price = best_price
                        self.bids.best_tid = self.bids.lob[best_price][1][0][2]
                else:
                        self.asks.book_add(order)
                        best_price = self.asks.lob_anon[0][0]
                        self.asks.best_price = best_price
                        self.asks.best_tid = self.asks.lob[best_price][1][0][2]

        
        def del_order(self, order):
                # delete an order from the exchange, update all internal records
                tid = order.tid
                if order.otype == 'Bid':
                        self.bids.book_del(order)
                        best_price = self.bids.lob_anon[-1][0]
                        self.bids.best_price = best_price
                        self.bids.best_tid = self.bids.lob[best_price][1][0][2]
                else:
                        self.asks.book_del(order)
                        best_price = self.asks.lob_anon[0][0]
                        self.asks.best_price = best_price
                        self.asks.best_tid = self.asks.lob[best_price][1][0][2]


        def process_order2(self, time, order, verbose):
                # receive an order and either add it to the relevant LOB (ie treat as limit order)
                # or if it crosses the best counterparty offer, execute (treat as a market order)
                oprice = order.price
                counterparty = None
                self.add_order(order)  # add it to the order lists -- overwriting any previous order
                best_ask = self.asks.best_price
                best_ask_tid = self.asks.best_tid
                best_bid = self.bids.best_price
                best_bid_tid = self.bids.best_tid
                if order.otype == 'Bid':
                        if self.asks.n_orders > 0 and best_bid >= best_ask:
                                # bid hits the best ask
                                if verbose: print("Bid hits best ask")
                                counterparty = best_ask_tid
                                price = best_ask  # bid crossed ask, so use ask price
                                if verbose: print('counterparty, price', counterparty, price)
                                # delete the ask just crossed
                                self.asks.delete_best()
                                # delete the bid that was the latest order
                                self.bids.delete_best()
                elif order.otype == 'Ask':
                        if self.bids.n_orders > 0 and best_ask <= best_bid:
                                # ask hits the best bid
                                if verbose: print("Ask hits best bid")
                                # remove the best bid
                                counterparty = best_bid_tid
                                price = best_bid  # ask crossed bid, so use bid price
                                if verbose: print('counterparty, price', counterparty, price)
                                # delete the bid just crossed, from the exchange's records
                                self.bids.delete_best()
                                # delete the ask that was the latest order, from the exchange's records
                                self.asks.delete_best()
                else:
                        # we should never get here
                        sys.exit('process_order() given neither Bid nor Ask')
                # NB at this point we have deleted the order from the exchange's records
                # but the two traders concerned still have to be notified
                if counterparty != None:
                        # process the trade
                        if verbose: print('>>>>>>>>>>>>>>>>>TRADE t=%5.2f $%d %s %s' % (time, price, counterparty, order.tid))
                        transaction_record = {'time': time,
                                               'price': price,
                                               'party1':counterparty,
                                               'party2':order.tid,
                                               'qty': order.qty}
                        self.tape.append(transaction_record)
                        return transaction_record
                else:
                        return None



        def tape_dump(self, fname, fmode, tmode):
                dumpfile = open(fname, fmode)
                for tapeitem in self.tape:
                        dumpfile.write('%s, %s\n' % (tapeitem['time'], tapeitem['price']))
                dumpfile.close()
                if tmode == 'wipe':
                        self.tape = []


        # this returns the LOB data "published" by the exchange,
        # i.e., what is accessible to the traders
        def publish_lob(self, time, verbose):
                public_data = {}
                public_data['time'] = time
                public_data['bids'] = {'best':self.bids.best_price,
                                     'worst':self.bids.worstprice,
                                     'n': self.bids.n_orders,
                                     'lob':self.bids.lob_anon}
                public_data['asks'] = {'best':self.asks.best_price,
                                     'worst':self.asks.worstprice,
                                     'n': self.asks.n_orders,
                                     'lob':self.asks.lob_anon}
                if verbose:
                        print('publish_lob: t=%d' % time)
                        print('BID_lob=%s' % public_data['bids']['lob'])
                        print('ASK_lob=%s' % public_data['asks']['lob'])
                return public_data






##################--Traders below here--#############


# Trader superclass
# all Traders have a trader id, bank balance, blotter, and list of orders to execute
class Trader:

        def __init__(self, ttype, tid, balance):
                self.ttype = ttype
                self.tid = tid
                self.balance = balance
                self.blotter = []
                self.orders = []
                self.willing = 1
                self.able = 1
                self.lastquote = None


        def __str__(self):
                return '[TID %s type %s balance %s blotter %s orders %s]' % (self.tid, self.ttype, self.balance, self.blotter, self.orders)


        def add_order(self, order):
                # in this version, trader has at most one order,
                # if allow more than one, this needs to be self.orders.append(order)
                self.orders = [order]


        def del_order(self, order):
                # this is lazy: assumes each trader has only one order with quantity=1, so deleting sole order
                # CHANGE TO DELETE THE HEAD OF THE LIST AND KEEP THE TAIL
                self.orders = []


        def bookkeep(self, trade, order, verbose):

                outstr = '%s (%s) bookkeeping: orders=' % (self.tid, self.ttype)
                for order in self.orders: outstr = outstr + str(order)

                self.blotter.append(trade)  # add trade record to trader's blotter
                # NB What follows is **LAZY** -- assumes all orders are quantity=1
                transactionprice = trade['price']
                if self.orders[0].otype == 'Bid':
                        profit = self.orders[0].price - transactionprice
                else:
                        profit = transactionprice - self.orders[0].price
                self.balance += profit
                if verbose: print('%s profit=%d balance=%d ' % (outstr, profit, self.balance))
                self.del_order(order)  # delete the order


        # specify how trader responds to events in the market
        # this is a null action, expect it to be overloaded with clever things by specific algos
        def respond(self, time, lob, trade, verbose):
                return None




# Trader subclass Giveaway
# even dumber than a ZI-U: just give the deal away
# (but never makes a loss)
class Trader_Giveaway(Trader):

        def getorder(self, time, countdown, lob):
                if len(self.orders) < 1:
                        order = None
                else:
                        quoteprice = self.orders[0].price
                        self.lastquote = quoteprice
                        order = Order(self.tid,
                                    self.orders[0].otype,
                                    quoteprice,
                                    self.orders[0].qty,
                                    time)
                return order



# Trader subclass ZI-C
# After Gode & Sunder 1993
class Trader_ZIC(Trader):

        def getorder(self, time, countdown, lob):
                if len(self.orders) < 1:
                        # no orders: return NULL
                        order = None
                else:
                        minprice = lob['bids']['worst']
                        maxprice = lob['asks']['worst']
                        limit = self.orders[0].price
                        otype = self.orders[0].otype
                        if otype == 'Bid':
                                quoteprice = random.randint(minprice, limit)
                        else:
                                quoteprice = random.randint(limit, maxprice)
                                # NB should check it == 'Ask' and barf if not
                        order = Order(self.tid, otype, quoteprice, self.orders[0].qty, time)

                return order


# Trader subclass Shaver
# shaves a penny off the best price
# if there is no best price, creates "stub quote" at system max/min
class Trader_Shaver(Trader):

        def getorder(self, time, countdown, lob):
                if len(self.orders) < 1:
                        order = None
                else:
                        limitprice = self.orders[0].price
                        otype = self.orders[0].otype
                        if otype == 'Bid':
                                if lob['bids']['n'] > 0:
                                        quoteprice = lob['bids']['best'] + 1
                                        if quoteprice > limitprice :
                                                quoteprice = limitprice
                                else:
                                        quoteprice = lob['bids']['worst']
                        else:
                                if lob['asks']['n'] > 0:
                                        quoteprice = lob['asks']['best'] - 1
                                        if quoteprice < limitprice:
                                                quoteprice = limitprice
                                else:
                                        quoteprice = lob['asks']['worst']
                        self.lastquote = quoteprice
                        order = Order(self.tid, otype, quoteprice, self.orders[0].qty, time)

                return order


# Trader subclass Sniper
# Based on Shaver,
# "lurks" until time remaining < threshold% of the trading session
# then gets increasing aggressive, increasing "shave thickness" as time runs out
class Trader_Sniper(Trader):

        def getorder(self, time, countdown, lob):
                lurk_threshold = 0.2
                shavegrowthrate = 3
                shave = int(1.0 / (0.01 + countdown / (shavegrowthrate * lurk_threshold)))
                if (len(self.orders) < 1) or (countdown > lurk_threshold):
                        order = None
                else:
                        limitprice = self.orders[0].price
                        otype = self.orders[0].otype

                        if otype == 'Bid':
                                if lob['bids']['n'] > 0:
                                        quoteprice = lob['bids']['best'] + shave
                                        if quoteprice > limitprice :
                                                quoteprice = limitprice
                                else:
                                        quoteprice = lob['bids']['worst']
                        else:
                                if lob['asks']['n'] > 0:
                                        quoteprice = lob['asks']['best'] - shave
                                        if quoteprice < limitprice:
                                                quoteprice = limitprice
                                else:
                                        quoteprice = lob['asks']['worst']
                        self.lastquote = quoteprice
                        order = Order(self.tid, otype, quoteprice, self.orders[0].qty, time)

                return order


# Trader subclass Sniper2
# Based on http://researcher.ibm.com/researcher/files/us-kephart/ec01_dblauc.pdf Sniper section.
# Goes for three types of trade:
#       Juicy Offer: best ask < minimum trade in previous period.
#       Small spread: best ask < max trade price prev period and
#               ratio of bid-ask spread and best ask < spread factor F_s and
#               expected profit > min profit factor F_p
#       Time running out: fraction time remaining < F_t
# Paper gives values F_s = 0.025, F_p = 0.02 and F_t = 0.1 with each parameter randomized +-50%

class Trader_Sniper2(Trader):
        def __init__(self, ttype, tid, balance):
                self.ttype = ttype
                self.tid = tid
                self.balance = balance
                self.blotter = []
                self.orders = []
                self.f_s = 0.025 * random.uniform(0.5,1.5)
                self.f_t = 0.1 * random.uniform(0.5,1.5)
                self.f_p = 0.02 * random.uniform(0.5,1.5)
                self.interval_length = 30
                self.min_trade_last_period = 0
                self.max_trade_last_period = 0
                self.best_trade_this_period = float('inf')
                self.worst_trade_this_period = float('-inf')
                self.interval = 1
                return

        def getorder(self, time, countdown, lob):

                if len(self.orders) < 1:
                        # no orders: return NULL
                        order = None
                else:
                        minprice = lob['bids']['worst']
                        maxprice = lob['asks']['worst']
                        oprice = self.orders[0].price
                        otype = self.orders[0].otype
                        # Bid = I am buying
                        if otype == 'Bid':
                                if lob['asks']['n'] == 0:
                                        return
                                best = lob['asks']['best']
                                if best < oprice:
                                        # Look for a juicy one
                                        if best < self.min_trade_last_period:
                                                return Order(self.tid, otype, best, self.orders[0].qty, time)
                                        # Look for a small spread one
                                        if best < self.max_trade_last_period:
                                                if lob['bids']['best'] != None:
                                                        bid_ask_spread = lob['asks']['best'] - lob['bids']['best']
                                                        ratio = float(bid_ask_spread)/float(best)
                                                        #print(ratio)
                                                        # If the spread is small enough
                                                        if ratio < self.f_s:
                                                                # and we will make enough profit
                                                                exp_profit = (oprice-best)/float(oprice)
                                                                if exp_profit > (1+self.f_p):
                                                                        return Order(self.tid, otype, best,\
                                                                        self.orders[0].qty, time)
                                        # PANIC AH AHHHHH OH MY GOD AHHHHH!!!!!!!!!!!!!!
                                        if countdown < self.f_t:
                                                return Order(self.tid, otype, best, self.orders[0].qty, time)
                        # We are a seller.
                        else:
                                if lob['bids']['n'] == 0:
                                        return
                                best = lob['bids']['best']
                                if best > oprice:
                                        # Look for a juicy one
                                        if best > self.max_trade_last_period:
                                                return Order(self.tid, otype, best, self.orders[0].qty, time)
                                        # Look for a small spread one
                                        if best > self.min_trade_last_period:
                                                if lob['asks']['best'] != None:
                                                        bid_ask_spread = lob['asks']['best'] - lob['bids']['best']
                                                        ratio = float(bid_ask_spread)/float(lob['asks']['best'])
                                                        #print(ratio)
                                                        # If the spread is small enough
                                                        if ratio < self.f_s:
                                                                # and we will make enough profit
                                                                exp_profit = (best-oprice)/float(oprice)
                                                                if exp_profit > (1+self.f_p):
                                                                        return Order(self.tid, otype, best,\
                                                                        self.orders[0].qty, time)
                                        # PANIC AH AHHHHH OH MY GOD AHHHHH!!!!!!!!!!!!!!
                                        if countdown < self.f_t:
                                                return Order(self.tid, otype, best, self.orders[0].qty, time)
                                return

        def respond(self, time, lob, trade, verbose):
                def change_period(time):
                        # Then we're in a new interval.
                        if time > (self.interval_length*self.interval):
                                self.min_trade_last_period = self.best_trade_this_period
                                self.best_trade_this_period = float('inf')
                                self.max_trade_last_period = self.worst_trade_this_period
                                self.worst_trade_this_period = float('-inf')
                                self.interval = self.interval + 1

                def trade_better(trade):
                        if trade != None:
                                if trade['price'] < self.best_trade_this_period:
                                        self.best_trade_this_period = trade['price']
                                if trade['price'] > self.worst_trade_this_period:
                                        self.worst_trade_this_period = trade['price']

                trade_better(trade)
                change_period(time)


# Trader subclass Adaptive Sniper
# based on the paper "Adaptive Sniping for Volatile and Stable Continuous Double Auction Markets"
class Trader_Adaptive_Sniper(Trader):
        def __init__(self, ttype, tid, balance):
                self.ttype = ttype
                self.tid = tid
                self.balance = balance
                self.blotter = []
                self.orders = []
                self.A = 0.6 * random.uniform(0.75,1.25)
                self.N = 0.25 * random.uniform(0.75,1.25)
                self.J = 0.1 * random.uniform(0.75,1.25)
                self.panic = random.uniform(0.1,0.2)
                self.beta = 0.2
                self.interval_length = 30
                self.first_period = True
                self.trades_this_period = 0
                self.trades_last_period = 0
                self.interval = 1
                self.global_trade_list = []
                self.trade_gap_list = []
                self.last_trade_gap = 0
                self.opp = []
                self.keep_history_for_periods = 3
                return

        def getorder(self, time, countdown, lob):

                def prob_gap_trade(gap):
                        mean = scipy.mean(self.trade_gap_list)
                        res =  math.exp(-1*gap/float(mean))
                        return res

                def prob_trade_occuring(price):
                        # Work out the chance of the trade occuring at <= 'price'
                        # assuming normal distribution N~(mean,var)
                        #print(self.global_trade_list)
                        mean = scipy.mean(self.global_trade_list)
                        var = scipy.std(self.global_trade_list)
                        dist = scipy.stats.norm(mean, var)
                        return dist.cdf(price)

                if len(self.orders) < 1:
                        # no orders: return NULL
                        order = None
                else:
                        oprice = self.orders[0].price
                        otype = self.orders[0].otype
                        # If Bid, then I am a buyer!
                        if otype == 'Bid':
                                if lob['asks']['n'] == 0 or lob['bids']['n'] == 0:
                                        return


                                o_a = lob['asks']['best']
                                o_b = lob['bids']['best']

                                if o_a > oprice:
                                        return

                                if self.trades_last_period == 0:
                                        if countdown < self.panic:
                                                # Panicking
                                                return Order(self.tid, otype, o_a, self.orders[0].qty, time)
                                else:
                                        if (1-(self.trades_this_period/float(self.trades_last_period))) < self.panic:
                                                # Panicking
                                                return Order(self.tid, otype, o_a, self.orders[0].qty, time)

                                # Calm
                                # Firstly, log the "sniping opportunity when not panicking":
                                # as a 3-tuple <price,spread,unit>
                                # to be used to update A and N at the end of a period.
                                prob_o_a = prob_trade_occuring(o_a)
                                prob_gap = prob_gap_trade(o_a-o_b)
                                to_add = {"o_a":o_a,\
                                          "prob_gap":prob_gap,\
                                          "prob_o_a":prob_o_a,\
                                          "oprice":self.orders[0].price,\
                                          "time":self.orders[0].time,\
                                          "otype":otype}
                                self.opp.append(to_add)

                                # Juicy: if P(X < oa) < J
                                # work out P(X < oa) with X~N(mean, var) where mean is sample mean
                                # and the var is sample variance, where sample is all trade data
                                if prob_o_a < self.J:
                                        # we're juicy => snipe
                                        return Order(self.tid, otype, o_a, self.orders[0].qty, time)
                                elif prob_o_a < self.A and prob_gap > self.N:
                                        # we're attractive => snipe
                                        return Order(self.tid, otype, o_a, self.orders[0].qty, time)
                        # We are a seller.
                        else:
                                if lob['asks']['n'] == 0 or lob['bids']['n'] == 0:
                                        return

                                o_a = lob['asks']['best']
                                o_b = lob['bids']['best']

                                if o_b < oprice:
                                        return

                                if self.trades_last_period == 0:
                                        if countdown < self.panic:
                                                # Panicking
                                                return Order(self.tid, otype, o_b, self.orders[0].qty, time)
                                else:
                                        if (1-(self.trades_this_period/float(self.trades_last_period))) < self.panic:
                                                # Panicking
                                                return Order(self.tid, otype, o_b, self.orders[0].qty, time)


                                # Calm
                                # Firstly, log the "sniping opportunity when not panicking":
                                # as a 3-tuple <price,spread,unit>
                                # to be used to update A and N at the end of a period.
                                prob_o_b =  1 - prob_trade_occuring(o_b)
                                prob_gap = prob_gap_trade(o_a-o_b)
                                to_add = {"o_b":o_b,\
                                          "prob_gap":prob_gap,\
                                          "prob_o_b":prob_o_b,\
                                          "oprice":self.orders[0].price,\
                                          "time":self.orders[0].time,\
                                          "otype":otype}
                                self.opp.append(to_add)

                                # Juicy: if P(X < oa) < J
                                # work out P(X < oa) with X~N(mean, var) where mean is sample mean
                                # and the var is sample variance, where sample is all trade data
                                if prob_o_b < self.J:
                                        # we're juicy => snipe
                                        return Order(self.tid, otype, o_b, self.orders[0].qty, time)
                                elif prob_o_b < self.A and prob_gap > self.N:
                                        # we're attractive => snipe
                                        return Order(self.tid, otype, o_b, self.orders[0].qty, time)



        def respond(self, time, lob, trade, verbose):
                def change_period(time):
                        def update_A_N():                     
                                def my_range(start, end, step):
                                        while start <= end:
                                                yield start
                                                start += step

                                # Set up variables
                                profit_s = 0
                                A_s = 0.0
                                N_s = 0.0
                                n_s = 0
                                for A in my_range(0, 1, 0.1):
                                        for N in my_range(0, 1, 0.1):
                                                this_profit = 0
                                                i = 0
                                                while i < len(self.opp):
                                                        oprice = self.opp[i]['oprice']
                                                        prob_gap = self.opp[i]['prob_gap']
                                                        otype = self.opp[i]['otype']
                                                        if otype == 'Bid':
                                                                prob_o_a = self.opp[i]['prob_o_a']
                                                                o_a = self.opp[i]['o_a']
                                                                if o_a > oprice:
                                                                        # No trade is going to happen
                                                                        i = i + 1
                                                                        continue
                                                                else:
                                                                        if prob_o_a < self.J:
                                                                                # we're juicy => snipe
                                                                                this_profit = this_profit + (oprice-o_a)
                                                                                order_time_to_remove = self.opp[i]['time']
                                                                                while i < len(self.opp):
                                                                                        if self.opp[i]['time'] == order_time_to_remove:
                                                                                                i = i + 1
                                                                                        else:
                                                                                                break
                                                                        elif prob_o_a < A and prob_gap > N:
                                                                                this_profit = this_profit + (oprice-o_a)
                                                                                order_time_to_remove = self.opp[i]['time']
                                                                                while i < len(self.opp):
                                                                                        if self.opp[i]['time'] == order_time_to_remove:
                                                                                                i = i + 1
                                                                                        else:
                                                                                                break
                                                                        else:
                                                                                i = i + 1
                                                        else:
                                                                prob_o_b = self.opp[i]['prob_o_b']
                                                                o_b = self.opp[i]['o_b']
                                                                if o_b < oprice:
                                                                        # No trade is going to happen
                                                                        i = i + 1
                                                                        continue
                                                                else:
                                                                        if prob_o_b < self.J:
                                                                                # we're juicy => snipe
                                                                                this_profit = this_profit + (o_b-oprice)
                                                                                order_time_to_remove = self.opp[i]['time']
                                                                                while i < len(self.opp):
                                                                                        if self.opp[i]['time'] == order_time_to_remove:
                                                                                                i = i + 1
                                                                                        else:
                                                                                                break
                                                                        elif prob_o_b < A and prob_gap > N:
                                                                                this_profit = this_profit + (o_b-oprice)
                                                                                order_time_to_remove = self.opp[i]['time']
                                                                                while i < len(self.opp):
                                                                                        if self.opp[i]['time'] == order_time_to_remove:
                                                                                                i = i + 1
                                                                                        else:
                                                                                                break
                                                                        else:
                                                                                i = i + 1


                                                # Update A and N depending on whether this profit was any good
                                                if this_profit > profit_s:
                                                        profit_s = this_profit
                                                        A_s = A
                                                        N_s = N
                                                        n_s = n_s + 1
                                                elif this_profit == profit_s:
                                                        n_s = n_s + 1
                                                        delta_A_s = A - A_s
                                                        A_s = A_s + (delta_A_s/float(n_s))
                                                        delta_N_s = N - N_s
                                                        N_s = N_s + (delta_N_s/float(n_s))

                                if self.first_period == True:
                                        self.A = A_s
                                        self.N = N_s
                                else:
                                        self.A = self.A + (self.beta*(A_s - self.A))
                                        self.N = self.N + (self.beta*(N_s - self.N))

                                #print self.A
                                #print self.N
                                return

                        # Cap the history length at 3 intervals.
                        def cap_history(time, interval_length):
                                while len(self.opp) > 0:
                                        if self.opp[0]['time'] < time-(interval_length*self.keep_history_for_periods):
                                                self.opp.pop(0)
                                        else:
                                                break
                                return

                        # Are we in a new interval?
                        next_interval = self.interval_length*self.interval
                        if time > next_interval:
                                # Need to find new A and N using opp.
                                cap_history(time, self.interval_length)
                                update_A_N()

                                self.first_period = False

                                for x in range(0,self.trades_last_period):
                                        self.global_trade_list.pop(0)

                                for x in range(0,self.trades_last_period):
                                        self.trade_gap_list.pop(0)

                                # Reset things keeping information about the period.
                                self.trades_last_period = self.trades_this_period
                                self.trades_this_period = 0
                                self.opp = []


                                self.interval = self.interval + 1

                # Update objects which require information from a trade.
                def update_trade(trade, lob):
                        if trade != None:
                                # Note down all trade prices.
                                # Will be used in the calculation of trade probabilities to work
                                # out the chance of a trade at a certain value (for juicy/attractive)
                                self.global_trade_list.append(trade['price'])
                                self.trade_gap_list.append(self.last_trade_gap)

                                # Update trades this period value, so that we can use it to know when to panic
                                self.trades_this_period = self.trades_this_period + 1
                        else:
                                # We need to note the gap of this non-trade so if there is one in the next respond
                                # we can note it. Needed for the narrow gap criterion.
                                if lob['asks']['best'] == None or lob['bids']['best'] == None:
                                        return
                                else:
                                        self.last_trade_gap = lob['asks']['best'] - lob['bids']['best']

                update_trade(trade, lob)
                change_period(time)



# Trader subclass ZIP
# After Cliff 1997
class Trader_ZIP(Trader):

        # ZIP init key param-values are those used in Cliff's 1997 original HP Labs tech report
        # NB this implementation keeps separate margin values for buying & sellling,
        #    so a single trader can both buy AND sell
        #    -- in the original, traders were either buyers OR sellers

        def __init__(self, ttype, tid, balance):
                self.ttype = ttype
                self.tid = tid
                self.balance = balance
                self.blotter = []
                self.orders = []
                self.job = None  # this gets switched to 'Bid' or 'Ask' depending on order-type
                self.active = False  # gets switched to True while actively working an order
                self.prev_change = 0  # this was called last_d in Cliff'97
                self.beta = 0.1 + 0.4 * random.random()
                self.momntm = 0.1 * random.random()
                self.ca = 0.05  # self.ca & .cr were hard-coded in '97 but parameterised later
                self.cr = 0.05
                self.margin = None  # this was called profit in Cliff'97
                self.margin_buy = -1.0 * (0.05 + 0.3 * random.random())
                self.margin_sell = 0.05 + 0.3 * random.random()
                self.price = None
                self.limit = None
                # memory of best price & quantity of best bid and ask, on LOB on previous update
                self.prev_best_bid_p = None
                self.prev_best_bid_q = None
                self.prev_best_ask_p = None
                self.prev_best_ask_q = None


        def getorder(self, time, countdown, lob):
                if len(self.orders) < 1:
                        self.active = False
                        order = None
                else:
                        self.active = True
                        self.limit = self.orders[0].price
                        self.job = self.orders[0].otype
                        if self.job == 'Bid':
                                # currently a buyer (working a bid order)
                                self.margin = self.margin_buy
                        else:
                                # currently a seller (working a sell order)
                                self.margin = self.margin_sell
                        quoteprice = int(self.limit * (1 + self.margin))
                        self.price = quoteprice

                        order = Order(self.tid, self.job, quoteprice, self.orders[0].qty, time)

                return order


        # update margin on basis of what happened in market
        def respond(self, time, lob, trade, verbose):
                # ZIP trader responds to market events, altering its margin
                # does this whether it currently has an order to work or not

                def target_up(price):
                        # generate a higher target price by randomly perturbing given price
                        ptrb_abs = self.ca * random.random()  # absolute shift
                        ptrb_rel = price * (1.0 + (self.cr * random.random()))  # relative shift
                        target = int(round(ptrb_rel + ptrb_abs, 0))
# #                        print('TargetUp: %d %d\n' % (price,target))
                        return(target)


                def target_down(price):
                        # generate a lower target price by randomly perturbing given price
                        ptrb_abs = self.ca * random.random()  # absolute shift
                        ptrb_rel = price * (1.0 - (self.cr * random.random()))  # relative shift
                        target = int(round(ptrb_rel - ptrb_abs, 0))
# #                        print('TargetDn: %d %d\n' % (price,target))
                        return(target)


                def willing_to_trade(price):
                        # am I willing to trade at this price?
                        willing = False
                        if self.job == 'Bid' and self.active and self.price >= price:
                                willing = True
                        if self.job == 'Ask' and self.active and self.price <= price:
                                willing = True
                        return willing


                def profit_alter(price):
                        oldprice = self.price
                        diff = price - oldprice
                        change = ((1.0 - self.momntm) * (self.beta * diff)) + (self.momntm * self.prev_change)
                        self.prev_change = change
                        newmargin = ((self.price + change) / self.limit) - 1.0

                        if self.job == 'Bid':
                                if newmargin < 0.0 :
                                        self.margin_buy = newmargin
                                        self.margin = newmargin
                        else :
                                if newmargin > 0.0 :
                                        self.margin_sell = newmargin
                                        self.margin = newmargin

                        # set the price from limit and profit-margin
                        self.price = int(round(self.limit * (1.0 + self.margin), 0))
# #                        print('old=%d diff=%d change=%d price = %d\n' % (oldprice, diff, change, self.price))


                # what, if anything, has happened on the bid LOB?
                bid_improved = False
                bid_hit = False
                lob_best_bid_p = lob['bids']['best']
                lob_best_bid_q = None
                if lob_best_bid_p != None:
                        # non-empty bid LOB
                        lob_best_bid_q = lob['bids']['lob'][-1][1]
                        if self.prev_best_bid_p < lob_best_bid_p :
                                # best bid has improved
                                # NB doesn't check if the improvement was by self
                                bid_improved = True
                        elif trade != None and ((self.prev_best_bid_p > lob_best_bid_p) or ((self.prev_best_bid_p == lob_best_bid_p) and (self.prev_best_bid_q > lob_best_bid_q))):
                                # previous best bid was hit
                                bid_hit = True
                elif self.prev_best_bid_p != None:
                        # the bid LOB has been emptied by a hit
                                bid_hit = True

                # what, if anything, has happened on the ask LOB?
                ask_improved = False
                ask_lifted = False
                lob_best_ask_p = lob['asks']['best']
                lob_best_ask_q = None
                if lob_best_ask_p != None:
                        # non-empty ask LOB
                        lob_best_ask_q = lob['asks']['lob'][0][1]
                        if self.prev_best_ask_p > lob_best_ask_p :
                                # best ask has improved -- NB doesn't check if the improvement was by self
                                ask_improved = True
                        elif trade != None and ((self.prev_best_ask_p < lob_best_ask_p) or ((self.prev_best_ask_p == lob_best_ask_p) and (self.prev_best_ask_q > lob_best_ask_q))):
                                # trade happened and best ask price has got worse, or stayed same but quantity reduced -- assume previous best ask was lifted
                                ask_lifted = True
                elif self.prev_best_ask_p != None:
                        # the bid LOB is empty now but was not previously, so must have been hit
                                ask_lifted = True


                if verbose and (bid_improved or bid_hit or ask_improved or ask_lifted):
                        print ('B_improved', bid_improved, 'B_hit', bid_hit, 'A_improved', ask_improved, 'A_lifted', ask_lifted)


                deal = bid_hit or ask_lifted

                if self.job == 'Ask':
                        # seller
                        if deal :
                                tradeprice = trade['price']
                                if self.price <= tradeprice:
                                        # could sell for more? raise margin
                                        target_price = target_up(tradeprice)
                                        profit_alter(target_price)
                                elif ask_lifted and self.active and not willing_to_trade(tradeprice):
                                        # wouldnt have got this deal, still working order, so reduce margin
                                        target_price = target_down(tradeprice)
                                        profit_alter(target_price)
                        else:
                                # no deal: aim for a target price higher than best bid
                                if ask_improved and self.price > lob_best_ask_p:
                                        if lob_best_bid_p != None:
                                                target_price = target_up(lob_best_bid_p)
                                        else:
                                                target_price = lob['asks']['worst']  # stub quote
                                        profit_alter(target_price)

                if self.job == 'Bid':
                        # buyer
                        if deal :
                                tradeprice = trade['price']
                                if self.price >= tradeprice:
                                        # could buy for less? raise margin (i.e. cut the price)
                                        target_price = target_down(tradeprice)
                                        profit_alter(target_price)
                                elif bid_hit and self.active and not willing_to_trade(tradeprice):
                                        # wouldnt have got this deal, still working order, so reduce margin
                                        target_price = target_up(tradeprice)
                                        profit_alter(target_price)
                        else:
                                # no deal: aim for target price lower than best ask
                                if bid_improved and self.price < lob_best_bid_p:
                                        if lob_best_ask_p != None:
                                                target_price = target_down(lob_best_ask_p)
                                        else:
                                                target_price = lob['bids']['worst']  # stub quote
                                        profit_alter(target_price)

                
                # remember the best LOB data ready for next response
                self.prev_best_bid_p = lob_best_bid_p
                self.prev_best_bid_q = lob_best_bid_q
                self.prev_best_ask_p = lob_best_ask_p
                self.prev_best_ask_q = lob_best_ask_q

# Trader subclass MGD
# Implementation taken from Price formation in double auctions (1998), S. Gjerstad and J. Dickhaut.
class Trader_MGD(Trader):

        def __init__(self, ttype, tid, balance):
                self.memoryProperty = 4
                self.ttype = ttype
                self.tid = tid
                self.balance = balance
                self.blotter = []
                self.orders = []
                self.willing = 1
                self.able = 1
                self.lastquote = None
                self.quote = 0
                self.job = None
                self.best_belief = 0
                #self.tradeHistory = []
                self.lastLob = []
                self.highestTradePrice = bse_sys_maxprice
                self.lowestTradePrice = bse_sys_minprice
                self.currentHighest = bse_sys_maxprice
                self.currentLowest = bse_sys_minprice
                self.peroid = 0
                self.peroidSize = 30
                self.prev_best_bid = bse_sys_minprice
                self.prev_best_ask = bse_sys_maxprice
                self.improvement_history = []
                self.prevLob = []
                self.tradeCount = 0

        def _removeDuplicates(self, seq, idfun=None):
            # Taken From: http://www.peterbe.com/plog/uniqifiers-benchmark
            seen = set()
            if idfun is None:
                for x in seq:
                    if x in seen:
                        continue
                    seen.add(x)
                    yield x
            else:
                for x in seq:
                    x = idfun(x)
                    if x in seen:
                        continue
                    seen.add(x)
                    yield x


        def calcBestVal(self, lob):
                # Updates belief values then calculates best

                lob_best_bid_p = lob['bids']['best']
                lob_best_ask_p = lob['asks']['best']

                # Caculate Belief values
                BeliefValsX = []
                BeliefValsY = []

                tradesCount = 0
                pricesWithHistory = []

                TotalBids = []
                TotalAsks = []
                AcceptedAsks = []
                AcceptedBids = []
                RejectedAsks = []
                RejectedBids = []

                # Find Improvements and remove from trade history
                for improvement in self.improvement_history:
                        if (improvement[0] == 'ask_improved'):
                                RejectedAsks = RejectedAsks + [improvement[1]]
                                TotalAsks = TotalAsks + [improvement[1]]
                        elif (improvement[0] == 'bid_improved'):
                                RejectedBids = RejectedBids + [improvement[1]]
                                TotalBids = TotalBids + [improvement[1]]
                        elif (improvement[0] == 'bid_hit' or improvement[0] == 'ask_lifted'):
                                if (improvement[0] == 'bid_hit'):
                                        TotalBids = TotalBids + [improvement[1]]
                                        AcceptedBids = AcceptedBids + [improvement[1]]
                                else:
                                        TotalAsks = TotalAsks + [improvement[1]]
                                        AcceptedAsks = AcceptedAsks + [improvement[1]]

                TotalBids.sort()
                TotalAsks.sort()
                AcceptedAsks.sort()
                AcceptedBids.sort()
                RejectedAsks.sort()
                RejectedBids.sort()

                if self.job == 'Bid':
                        pricesWithHistory = self._removeDuplicates(TotalBids)
                        for price in pricesWithHistory:
                                if (price <= lob_best_bid_p):
                                        BeliefValsX.append(price)
                                        BeliefValsY.append(0.0)
                                # Trader is Buyer, calculate corresponding belief values                                        
                                # Accepted Bids Less or Equal for price k
                                ABL = sum((i <= price) for i in AcceptedBids)
                                # Asks Less or Equal for price k
                                AL = sum((i <= price) for i in TotalAsks)
                                # Unaccepted/Rejected Bids Greater or Equal for price k
                                UBG = sum((i >= price) for i in RejectedBids)

                                # Belief = ABL + AL / ABL + AL + UBG
                                BeliefValsX.append(price)   
                                BeliefValsY.append(float(ABL+AL)/float(ABL+AL+UBG))
                        # Need to Alter due to issues with interpolation generation
                        BeliefValsX.insert(0, bse_sys_minprice+1)
                        BeliefValsY.insert(0, 0.0)
                        BeliefValsX.insert(0, bse_sys_minprice)
                        BeliefValsY.insert(0, 0.0)
                        BeliefValsX.append(bse_sys_maxprice-1)
                        BeliefValsY.append(1.0)
                        BeliefValsX.append(bse_sys_maxprice)
                        BeliefValsY.append(1.0)        
                elif self.job == 'Ask':
                        pricesWithHistory = self._removeDuplicates(TotalAsks)
                        #print(pricesWithHistory)
                        for price in pricesWithHistory:
                                if (price >= lob_best_ask_p):
                                        BeliefValsX.append(price)
                                        BeliefValsY.append(0.0)
                                # Trader is Seller, calculate corresponding belief values
                                # Accepted Asks Greater or Equal for price 
                                AAG = sum((i >= price) for i in AcceptedAsks)
                                # Bids Greater or Equal for price 
                                BG = sum((i >= price) for i in TotalBids)
                                # Unaccepted/Rejected Asks Less or Equal for price 
                                UAL = sum((i <= price) for i in RejectedAsks)

                                # Belief = AAG + BG / AAG + BG + UAL
                                BeliefValsX.append(price)
                                BeliefValsY.append(float(AAG+BG)/float(AAG+BG+UAL))
                        # Need to Alter due to issues with interpolation generation
                        BeliefValsX.insert(0, bse_sys_minprice+1)
                        BeliefValsY.insert(0, 1.0)
                        BeliefValsX.insert(0, bse_sys_minprice)
                        BeliefValsY.insert(0, 1.0)
                        BeliefValsX.append(bse_sys_maxprice-1)
                        BeliefValsY.append(0.0)
                        BeliefValsX.append(bse_sys_maxprice)
                        BeliefValsY.append(0.0)


                # Do Iterpolations for each pair of BeliefVals:
                # print(lob)
                #print('X:')
                #print(BeliefValsX)
                #print('Y:')
                #print(BeliefValsY)
                f = interp1d(BeliefValsX, BeliefValsY)

                bestPrice = 0
                bestSurplus = 0

                # maximizes its expected surplus
                if self.job == 'Bid':
                        # currently a buyer (working a bid order)
                        for i in range(bse_sys_minprice, self.limit):
                                # belief * gain = belief * (limit-i)
                                belief = f(i)
                                if (belief < 0):
                                        #print("BAD - bid belief < 0")
                                        belief = 0
                                if (belief > 1):
                                        #print("BAD - bid belief > 0")
                                        belief = 1
                                if (i > self.currentHighest):
                                        belief = 1.0
                                if (i < self.currentLowest):
                                        belief = 0.0
                                surplus = (float(belief)*float(self.limit-i))
                                if surplus > bestSurplus:
                                        bestPrice = i
                                        bestSurplus = surplus
                elif self.job == 'Ask':
                        # currently a seller (working a sell order)
                        for j in range(self.limit+1, bse_sys_maxprice):
                                # belief * gain = belief * (limit-i)
                                belief = f(j)
                                if (belief < 0):
                                        #print("BAD - ask belief < 0")
                                        belief = 0
                                if (belief > 1):
                                        #print("BAD - ask belief > 0")
                                        belief = 1
                                if (j > self.currentHighest):
                                        belief = 0.0
                                if (j < self.currentLowest):
                                       belief = 1.0
                                surplus = (float(belief)*float(j-self.limit))
                                #print(j, belief, surplus)
                                if surplus > bestSurplus:
                                        bestPrice = j
                                        bestSurplus = surplus

                #print(bestPrice)
                return


        def getorder(self, time, countdown, lob):
                order = None

                if (self.orders != None) and (len(self.orders) > 0):
                        self.limit = self.orders[0].price
                        self.job = self.orders[0].otype                        
                        if (self.tradeCount > 1) and (self.job != None):
                                self.calcBestVal(lob)
                                if self.quote != 0:
                                        if (self.job == 'Ask' and bestPrice > self.limit):
                                                order=Order(self.tid, self.job, self.quote, 1, time)
                                        elif (self.job == 'Bid' and bestPrice < self.limit):
                                                order=Order(self.tid, self.job, self.quote, 1, time)


                return order

        # update currentQuote on basis of what happened in market
        def respond(self, time, lob, trade, verbose):

                if (self.peroidSize*self.peroid + time > self.peroidSize):
                        self.peroid = self.peroid + 1
                        self.highestTradePrice = self.currentHighest
                        self.lowestTradePrice = self.currentLowest
                        self.currentHighest = bse_sys_maxprice
                        self.currentLowest = bse_sys_minprice

                if (trade != None):        
                        self.tradeCount = self.tradeCount + 1
                        #self.tradeHistory.append([trade, self.prevLob])
                        if ((trade['price'] > self.currentHighest) and trade['price'] > self.highestTradePrice):
                                self.currentHighest = trade['price']
                        elif ((trade['price'] < self.currentLowest) and trade['price'] < self.lowestTradePrice):
                                self.currentLowest = trade['price']
                        elif (trade['price'] == self.prev_best_ask):
                                # Ask Lifted
                                self.improvement_history.append(['ask_lifted', trade['price']])
                        elif (trade['price'] == self.prev_best_bid):
                                # Bid Hit                                                
                                self.improvement_history.append(['bid_hit', trade['price']])
                else:
                        if (self.prev_best_ask == None and lob['asks']['best'] != None):
                                # Ask Improved
                                self.improvement_history.append(['ask_improved', lob['asks']['best']])
                        if  (self.prev_best_bid == None and lob['bids']['best'] != None):
                                # Bid Improved
                                self.improvement_history.append(['bid_improved', lob['bids']['best']])      
                        if (lob['asks']['best'] != None) and (lob['asks']['best'] != self.prev_best_ask):
                                # Ask Improved
                                self.improvement_history.append(['ask_improved', lob['asks']['best']])
                        if (lob['bids']['best'] != None) and (lob['bids']['best'] != self.prev_best_bid):
                                # Bid Improved
                                self.improvement_history.append(['bid_improved', lob['bids']['best']])

                while (self.tradeCount > self.memoryProperty):
                        #self.tradeHistory.pop(0)
                        for improvement in self.improvement_history:
                                self.improvement_history.pop(0)
                                if ((improvement[0] == 'bid_hit') or (improvement[0] == 'ask_lifted')):
                                        self.tradeCount = self.tradeCount - 1
                                        break

                #print(self.improvement_history)
                self.prevLob = lob
                self.prev_best_ask = lob['asks']['best']
                self.prev_best_bid = lob['bids']['best']
                return


class Trader_AA(Trader):

    def __init__(self, ttype, tid, balance):
        
        # External parameters (you must choose [optimise] values yourselves)
        self.spin_up_time = 5
        self.eta = 3.0
        #self.theta_max = 2
        self.theta_max = 2
        self.theta_min = -8
        self.lambda_a = 0.01
        self.lambda_r = 0.02
        self.beta_1 = 0.4
        self.beta_2 = 0.4
        self.gamma = 2.0
        self.nLastTrades = 5  # N in AIJ08
        self.ema_param = 2 / float(self.nLastTrades + 1)
        self.maxNewtonItter = 10
        self.maxNewtonError = 0.0001

        # The order we're trying to trade
        self.ttype = ttype
        self.tid = tid
        self.orders = []
        self.limit = None
        self.active = False
        self.job = None
        self.balance = balance
        self.blotter = []

        # Parameters describing what the market looks like and it's contstraints
        self.marketMax = bse_sys_maxprice
        self.prev_best_bid_p = None
        self.prev_best_bid_q = None
        self.prev_best_ask_p = None
        self.prev_best_ask_q = None

        # Internal parameters (spin up time need to get values for some of these)
        self.eqlbm = None
        self.theta = -1.0 * (5.0 * random.random())
        #self.theta = -4
        self.smithsAlpha = None
        self.lastTrades = []
        self.smithsAlphaMin = None
        self.smithsAlphaMax = None

        self.aggressiveness_buy = -1.0 * (0.3 * random.random())
        self.aggressiveness_sell = -1.0 * (0.3 * random.random())
        self.target_buy = None
        self.target_sell = None

    def updateEq(self, price):
        # Updates the equilibrium price estimate using EMA
        if self.eqlbm == None: 
            self.eqlbm = price
        else: 
            self.eqlbm = self.ema_param * price + (1 - self.ema_param) * self.eqlbm

    def newton4Buying(self):
        # runs Newton-Raphson to find theta_est (the value of theta that makes the 1st 
        # derivative of eqn(3) continuous)
        theta_est = self.theta
        rightHside = ((self.theta * (self.limit - self.eqlbm)) / float(math.exp(self.theta) - 1));
        i = 0
        while i <= self.maxNewtonItter:
            eX = math.exp(theta_est)
            eXminOne = eX - 1
            fofX = (((theta_est * self.eqlbm) / float(eXminOne)) - rightHside)
            if abs(fofX) <= self.maxNewtonError:
                break
            dfofX = ((self.eqlbm / eXminOne) - ((eX * self.eqlbm * theta_est) / float(eXminOne * eXminOne)))
            theta_est = (theta_est - (fofX / float(dfofX)));
            i += 1
        if theta_est == 0.0: theta_est += 0.000001
        return theta_est

    def newton4Selling(self):
        # runs Newton-Raphson to find theta_est (the value of theta that makes the 1st 
        # derivative of eqn(4) continuous)
        theta_est = self.theta
        rightHside = ((self.theta * (self.eqlbm - self.limit)) / float(math.exp(self.theta) - 1))
        i = 0
        while i <= self.maxNewtonItter:
            eX = math.exp(theta_est)
            eXminOne = eX - 1
            fofX = (((theta_est * (self.marketMax - self.eqlbm)) / float(eXminOne)) - rightHside)
            if abs(fofX) <= self.maxNewtonError:
                break
            dfofX = (((self.marketMax - self.eqlbm) / eXminOne) - ((eX * (self.marketMax - self.eqlbm) * theta_est) / float(eXminOne * eXminOne)))
            theta_est = (theta_est - (fofX / float(dfofX)))
            i += 1
        if theta_est == 0.0: theta_est += 0.000001
        return theta_est

    def updateTarget(self):

        # relates to eqns (3),(4),(5) and (6)
        # For buying
        if self.limit < self.eqlbm:
            # Extra-marginal buyer
            if self.aggressiveness_buy >= 0: 
                target = self.limit
            else: 
                target = self.limit * (1 - (math.exp(-self.aggressiveness_buy * self.theta) - 1) / float(math.exp(self.theta) - 1))
            self.target_buy = target
        else:
            # Intra-marginal buyer
            if self.aggressiveness_buy >= 0: 
                target = (self.eqlbm + (self.limit - self.eqlbm) * ((math.exp(self.aggressiveness_buy * self.theta) - 1) / float(math.exp(self.theta) - 1)))
            else:
                theta_est = self.newton4Buying()
                target = self.eqlbm * (1 - (math.exp(-self.aggressiveness_buy * theta_est) - 1) / float(math.exp(theta_est) - 1))
            self.target_buy = target
        # For selling
        if self.limit > self.eqlbm:
            # Extra-marginal seller
            if self.aggressiveness_sell >= 0: 
                target = self.limit
            else: 
                target = self.limit + (self.marketMax - self.limit) * ((math.exp(-self.aggressiveness_sell * self.theta) - 1) / float(math.exp(self.theta) - 1))
            self.target_sell = target
        else:
            # Intra-marginal seller
            if self.aggressiveness_sell >= 0: 
                target = self.limit + (self.eqlbm - self.limit) * (1 - (math.exp(self.aggressiveness_sell * self.theta) - 1) / float(math.exp(self.theta) - 1))
            else:
                theta_est = self.newton4Selling() 
                target = self.eqlbm + (self.marketMax - self.eqlbm) * ((math.exp(-self.aggressiveness_sell * theta_est) - 1) / (math.exp(theta_est) - 1))
            self.target_sell = target

    def calcRshout(self, target, buying):
        if buying:
            # Are we extramarginal?
            if self.eqlbm >= self.limit:
                r_shout = 0.0
            else:  # Intra-marginal
                if target > self.eqlbm:
                    if target > self.limit: 
                        target = self.limit
                    r_shout = math.log((((target - self.eqlbm) * (math.exp(self.theta) - 1)) / (self.limit - self.eqlbm)) + 1) / self.theta
                else:  # other formula for intra buyer
                    r_shout = math.log((1 - (target / self.eqlbm)) * (math.exp(self.newton4Buying()) - 1) + 1) / -self.newton4Buying()
        else:  # Selling
            # Are we extra-marginal?
            if self.limit >= self.eqlbm:
                r_shout = 0.0
            else:  # Intra-marginal
                if target > self.eqlbm:
                    r_shout = math.log(((target - self.eqlbm) * (math.exp(self.newton4Selling()) - 1)) / (self.marketMax - self.eqlbm) + 1) / -self.newton4Selling()
                else:  # other intra seller formula
                    if target < self.limit: 
                        target = self.limit
                    r_shout = math.log((1 - (target - self.limit) / (self.eqlbm - self.limit)) * (math.exp(self.theta) - 1) + 1) / self.theta
        return r_shout

    def updateAgg(self, up, buying, target):
        if buying:
            old_agg = self.aggressiveness_buy 
        else:
            old_agg = self.aggressiveness_sell
        if up:
            delta = (1 + self.lambda_r) * self.calcRshout(target, buying) + self.lambda_a
        else:
            delta = (1 - self.lambda_r) * self.calcRshout(target, buying) - self.lambda_a
        new_agg = old_agg + self.beta_1 * (delta - old_agg)
        if new_agg > 1.0: new_agg = 1.0
        elif new_agg < 0.0: new_agg = 0.000001
        return new_agg

    def updateSmithsAlpha(self, price):
        self.lastTrades.append(price)
        if not (len(self.lastTrades) <= self.nLastTrades):
             self.lastTrades.pop(0)
        self.smithsAlpha = math.sqrt(sum(((p - self.eqlbm) ** 2) for p in self.lastTrades) * (1 / float(len(self.lastTrades)))) / self.eqlbm
        
        if self.smithsAlphaMin == None:
            self.smithsAlphaMin = self.smithsAlpha
        elif self.smithsAlpha < self.smithsAlphaMin: 
            self.smithsAlphaMin = self.smithsAlpha
            
        if self.smithsAlphaMax == None:
            self.smithsAlphaMax = self.smithsAlpha
        elif self.smithsAlpha > self.smithsAlphaMax:
             self.smithsAlphaMax = self.smithsAlpha

    def updateTheta(self):
        if (self.smithsAlphaMin == self.smithsAlphaMax):
            alphaBar = 1.0
        else:
            alphaBar = (self.smithsAlpha - self.smithsAlphaMin) / (self.smithsAlphaMax - self.smithsAlphaMin)
            
        # delta_alpha = self.smithsAlphaMax - self.smithsAlphaMin
        # delta_theta = self.theta_max - self.theta_min
        
        # if (delta_alpha == 0):
        #     desiredTheta = (self.theta_max+self.theta_min)/2
        # else:
        #     desiredTheta = self.theta_min + delta_theta * \
        #     ( (1-math.exp(self.gamma*( ((self.smithsAlpha-self.smithsAlphaMin)/delta_alpha) - 1))) / (1-math.exp(-self.gamma)) )
        #     
        # #     
        #theta = (theta_max - theta_min) *  (1 - (alpha-alpha_min)/(alpha_max-alpha_min)) *  exp(gamma*((alpha-alpha_min)/(alpha_max-alpha_min) -1 ) ) +  theta_min
        desiredTheta = (self.theta_max - self.theta_min) * (1 - (alphaBar * math.exp(self.gamma * (alphaBar - 1)))) + self.theta_min
        theta = self.theta + self.beta_2 * (desiredTheta - self.theta)

        if theta == 0: 
             theta += 0.0000001
        self.theta = theta

    def getorder(self, time, countdown, lob):
        if len(self.orders) < 1:
            self.active = False
            order = None
        else:
            self.active = True
            self.limit = self.orders[0].price
            self.job = self.orders[0].otype
            if (self.eqlbm == None) or (self.prev_best_ask_p == None) or (self.prev_best_bid_p == None):
                return None
            self.updateTarget()
            if self.job == 'Bid':
                # currently a buyer (working a bid order)
                if self.spin_up_time > 0:
                    ask_plus = (1 + self.lambda_r) * self.prev_best_ask_p + self.lambda_a
                    quoteprice = self.prev_best_bid_p + (min(self.limit, ask_plus) - self.prev_best_bid_p) / self.eta
                else:
                    quoteprice = self.prev_best_bid_p + (self.target_buy - self.prev_best_bid_p) / self.eta
                # if self.orders[0].price > self.limit:
                #     print('OMFG ERRORZ')
            else:
                # currently a seller (working a sell order)
                if self.spin_up_time > 0:
                    bid_minus = (1 - self.lambda_r) * self.prev_best_bid_p - self.lambda_a
                    quoteprice = self.prev_best_ask_p - (self.prev_best_ask_p - max(self.limit, bid_minus)) / self.eta
                else:
                    quoteprice = (self.prev_best_ask_p - (self.prev_best_ask_p - self.target_sell) / self.eta)
                # if self.orders[0].price < self.limit:
                #     print('OMFG ERRORZ')
            order = Order(self.tid, self.job, quoteprice, self.orders[0].qty, time)

        return order             


    def respond(self, time, lob, trade, verbose):
        
        # if not self.printed:
        #         self.printed = True
        #         print(self.aggressiveness_buy, self.theta)
        # 
            
        # what, if anything, has happened on the bid LOB?
        bid_improved = False
        bid_hit = False
        lob_best_bid_p = lob['bids']['best']
        lob_best_bid_q = None
        if lob_best_bid_p != None:
            # non-empty bid LOB
            lob_best_bid_q = lob['bids']['lob'][-1][1]
            if self.prev_best_bid_p < lob_best_bid_p :
                # best bid has improved
                # NB doesn't check if the improvement was by self
                bid_improved = True
            elif trade != None and ((self.prev_best_bid_p > lob_best_bid_p) or ((self.prev_best_bid_p == lob_best_bid_p) and (self.prev_best_bid_q > lob_best_bid_q))):
                # previous best bid was hit
                bid_hit = True
        elif self.prev_best_bid_p != None:
            # the bid LOB has been emptied by a hit
                bid_hit = True

        # what, if anything, has happened on the ask LOB?
        ask_improved = False
        ask_lifted = False
        lob_best_ask_p = lob['asks']['best']
        lob_best_ask_q = None
        if lob_best_ask_p != None:
            # non-empty ask LOB
            lob_best_ask_q = lob['asks']['lob'][0][1]
            if self.prev_best_ask_p > lob_best_ask_p :
                # best ask has improved -- NB doesn't check if the improvement was by self
                ask_improved = True
            elif trade != None and ((self.prev_best_ask_p < lob_best_ask_p) or ((self.prev_best_ask_p == lob_best_ask_p) and (self.prev_best_ask_q > lob_best_ask_q))):
                # trade happened and best ask price has got worse, or stayed same but quantity reduced -- assume previous best ask was lifted
                ask_lifted = True
        elif self.prev_best_ask_p != None:
            # the bid LOB is empty now but was not previously, so must have been hit
            ask_lifted = True

        if verbose and (bid_improved or bid_hit or ask_improved or ask_lifted):
            print ('B_improved', bid_improved, 'B_hit', bid_hit, 'A_improved', ask_improved, 'A_lifted', ask_lifted)

        deal = bid_hit or ask_lifted
        self.prev_best_bid_p = lob_best_bid_p
        self.prev_best_ask_p = lob_best_ask_p

        if self.spin_up_time > 0: 
            self.spin_up_time -= 1
            return
            
        if (self.limit == None):
            return;
            
        if deal:
            self.updateEq(trade['price'])
            self.updateSmithsAlpha(trade['price'])
            self.updateTheta()
            
        if (self.eqlbm == None):
            return

        # The lines below represent the rules in fig(7) in AIJ08. The if statements have not
        # been merged for the sake of clarity.        
        # For buying
        if deal:
            if self.target_buy >= trade['price']: 
                self.aggressiveness_buy = self.updateAgg(False, True, trade['price'])
            else: 
                self.aggressiveness_buy = self.updateAgg(True, True, trade['price'])
        elif bid_improved and (self.target_buy <= self.prev_best_bid_p): 
            self.aggressiveness_buy = self.updateAgg(True, True, self.prev_best_bid_p)
        # For selling
        if deal:
            if self.target_sell <= trade['price']:  
                self.aggressiveness_sell = self.updateAgg(False, False, trade['price'])
            else: self.aggressiveness_sell = self.updateAgg(True, False, trade['price'])
        elif ask_improved and (self.target_sell >= self.prev_best_ask_p): 
            self.aggressiveness_sell = self.updateAgg(True, False, self.prev_best_ask_p)
        
        self.updateTarget()


##########################---trader-types have all been defined now--################




##########################---Below lies the experiment/test-rig---##################



# trade_stats()
# dump CSV statistics on exchange data and trader population to file for later analysis
# this makes no assumptions about the number of types of traders, or
# the number of traders of any one type -- allows either/both to change
# between successive calls, but that does make it inefficient as it has to
# re-analyse the entire set of traders on each call
def trade_stats(expid, traders, dumpfile, time, lob):
        trader_types = {}
        n_traders = len(traders)
        for t in traders:
                ttype = traders[    t].ttype
                if ttype in trader_types.keys():
                        t_balance = trader_types[ttype]['balance_sum'] + traders[t].balance
                        n = trader_types[ttype]['n'] + 1
                else:
                        t_balance = traders[t].balance
                        n = 1
                trader_types[ttype] = {'n':n, 'balance_sum':t_balance}


        dumpfile.write('%s, %06d, ' % (expid, time))
        for ttype in sorted(list(trader_types.keys())):
                n = trader_types[ttype]['n']
                s = trader_types[ttype]['balance_sum']
                dumpfile.write('%s, %d, %d, %f, ' % (ttype, s, n, s / float(n)))

        if lob['bids']['best'] != None :
                dumpfile.write('%d, ' % (lob['bids']['best']))
        else:
                dumpfile.write('N, ')
        if lob['asks']['best'] != None :
                dumpfile.write('%d, ' % (lob['asks']['best']))
        else:
                dumpfile.write('N, ')
        dumpfile.write('\n');





# create a bunch of traders from traders_spec
# returns tuple (n_buyers, n_sellers)
# optionally shuffles the pack of buyers and the pack of sellers
def populate_market(traders_spec, traders, shuffle, verbose):

        def trader_type(robottype, name):
                if robottype == 'GVWY':
                        return Trader_Giveaway('GVWY', name, 0.00)
                elif robottype == 'ZIC':
                        return Trader_ZIC('ZIC', name, 0.00)
                elif robottype == 'SHVR':
                        return Trader_Shaver('SHVR', name, 0.00)
                elif robottype == 'SNPR':
                        return Trader_Sniper2('SNPR2', name, 0.00)
                elif robottype == 'SNPR2':
                        return Trader_Adaptive_Sniper('SNP2', name, 0.00)
                elif robottype == 'AS':
                        return Trader_Sniper('AS', name, 0.00)
                elif robottype == 'ZIP':
                        return Trader_ZIP('ZIP', name, 0.00)
                elif robottype == 'MGD':
                        return Trader_MGD('MGD', name, 0.00)
                elif robottype == 'AA':
                        return Trader_AA('AA', name, 0.00)
                else:
                        sys.exit('FATAL: don\'t know robot type %s\n' % robottype)


        def shuffle_traders(ttype_char, n, traders):
                for swap in range(n):
                        t1 = (n - 1) - swap
                        t2 = random.randint(0, t1)
                        t1name = '%c%02d' % (ttype_char, t1)
                        t2name = '%c%02d' % (ttype_char, t2)
                        traders[t1name].tid = t2name
                        traders[t2name].tid = t1name
                        temp = traders[t1name]
                        traders[t1name] = traders[t2name]
                        traders[t2name] = temp


        n_buyers = 0
        for bs in traders_spec['buyers']:
                ttype = bs[0]
                for b in range(bs[1]):
                        tname = 'B%02d' % n_buyers  # buyer i.d. string
                        traders[tname] = trader_type(ttype, tname)
                        n_buyers = n_buyers + 1

        if n_buyers < 1:
                sys.exit('FATAL: no buyers specified\n')

        if shuffle: shuffle_traders('B', n_buyers, traders)


        n_sellers = 0
        for ss in traders_spec['sellers']:
                ttype = ss[0]
                for s in range(ss[1]):
                        tname = 'S%02d' % n_sellers  # buyer i.d. string
                        traders[tname] = trader_type(ttype, tname)
                        n_sellers = n_sellers + 1

        if n_sellers < 1:
                sys.exit('FATAL: no sellers specified\n')

        if shuffle: shuffle_traders('S', n_sellers, traders)

        if verbose :
                for t in range(n_buyers):
                        bname = 'B%02d' % t
                        print(traders[bname])
                for t in range(n_sellers):
                        bname = 'S%02d' % t
                        print(traders[bname])


        return {'n_buyers':n_buyers, 'n_sellers':n_sellers}



# customer_orders(): allocate orders to traders
# parameter "os" is order schedule
# os['timemode'] is either 'periodic', 'drip-fixed', 'drip-jitter', or 'drip-poisson'
# os['interval'] is number of seconds for a full cycle of replenishment
# drip-poisson sequences will be normalised to ensure time of last replenisment <= interval
# parameter "pending" is the list of future orders (if this is empty, generates a new one from os)
# revised "pending" is the returned value
#
# if a supply or demand schedule mode is "random" and more than one range is supplied in ranges[],
# then each time a price is generated one of the ranges is chosen equiprobably and
# the price is then generated uniform-randomly from that range
#
# if len(range)==2, interpreted as min and max values on the schedule, specifying linear supply/demand curve
# if len(range)==3, first two vals are min & max, third value should be a function that generates a dynamic price offset
#                   -- the offset value applies equally to the min & max, so gradient of linear sup/dem curve doesn't vary
# if len(range)==4, the third value is function that gives dynamic offset for schedule min,
#                   and fourth is a function giving dynamic offset for schedule max, so gradient of sup/dem linear curve can vary
#
# the interface on this is a bit of a mess... could do with refactoring


def customer_orders(time, last_update, traders, trader_stats, os, pending, verbose):


        def sysmin_check(price):
                if price < bse_sys_minprice:
                        print('WARNING: price < bse_sys_min -- clipped')
                        price = bse_sys_minprice
                return price


        def sysmax_check(price):
                if price > bse_sys_maxprice:
                        print('WARNING: price > bse_sys_max -- clipped')
                        price = bse_sys_maxprice
                return price

        

        def getorderprice(i, sched, n, mode, issuetime):
                # does the first schedule range include optional dynamic offset function(s)?
                if len(sched[0]) > 2:
                        offsetfn = sched[0][2]
                        if callable(offsetfn):
                                # same offset for min and max
                                offset_min = offsetfn(issuetime)
                                offset_max = offset_min
                        else:
                                sys.exit('FAIL: 3rd argument of sched in getorderprice() not callable')
                        if len(sched[0]) > 3:
                                # if second offset function is specfied, that applies only to the max value
                                offsetfn = sched[0][3]
                                if callable(offsetfn):
                                        # this function applies to max
                                        offset_max = offsetfn(issuetime)
                                else:
                                        sys.exit('FAIL: 4th argument of sched in getorderprice() not callable')
                else:
                        offset_min = 0.0
                        offset_max = 0.0

                pmin = sysmin_check(offset_min + min(sched[0][0], sched[0][1]))
                pmax = sysmax_check(offset_max + max(sched[0][0], sched[0][1]))
                prange = pmax - pmin
                stepsize = prange / (n - 1)
                halfstep = round(stepsize / 2.0)

                if mode == 'fixed':
                        orderprice = pmin + int(i * stepsize) 
                elif mode == 'jittered':
                        orderprice = pmin + int(i * stepsize) + random.randint(-halfstep, halfstep)
                elif mode == 'random':
                        if len(sched) > 1:
                                # more than one schedule: choose one equiprobably
                                s = random.randint(0, len(sched) - 1)
                                pmin = sysmin_check(min(sched[s][0], sched[s][1]))
                                pmax = sysmax_check(max(sched[s][0], sched[s][1]))
                        orderprice = random.randint(pmin, pmax)
                else:
                        sys.exit('FAIL: Unknown mode in schedule')
                orderprice = sysmin_check(sysmax_check(orderprice))
                return orderprice



        def getissuetimes(n_traders, mode, interval, shuffle, fittointerval):
                interval = float(interval)
                if n_traders < 1:
                        sys.exit('FAIL: n_traders < 1 in getissuetime()')
                elif n_traders == 1:
                        tstep = interval
                else:
                        tstep = interval / (n_traders - 1)
                arrtime = 0
                issuetimes = []
                for t in range(n_traders):
                        if mode == 'periodic':
                                arrtime = interval
                        elif mode == 'drip-fixed':
                                arrtime = t * tstep
                        elif mode == 'drip-jitter':
                                arrtime = t * tstep + tstep * random.random()
                        elif mode == 'drip-poisson':
                                # poisson requires a bit of extra work
                                interarrivaltime = random.expovariate(n_traders / interval)
                                arrtime += interarrivaltime
                        else:
                                sys.exit('FAIL: unknown time-mode in getissuetimes()')
                        issuetimes.append(arrtime) 
                        
                # at this point, arrtime is the last arrival time
                if fittointerval and ((arrtime > interval) or (arrtime < interval)):
                        # generated sum of interarrival times longer than the interval
                        # squish them back so that last arrival falls at t=interval
                        for t in range(n_traders):
                                issuetimes[t] = interval * (issuetimes[t] / arrtime)
                # optionally randomly shuffle the times
                if shuffle:
                        for t in range(n_traders):
                                i = (n_traders - 1) - t
                                j = random.randint(0, i)
                                tmp = issuetimes[i]
                                issuetimes[i] = issuetimes[j]
                                issuetimes[j] = tmp
                return issuetimes
        

        def getschedmode(time, os):
                got_one = False
                for sched in os:
                        if (sched['from'] <= time) and (time < sched['to']) :
                                # within the timezone for this schedule
                                schedrange = sched['ranges']
                                mode = sched['stepmode']
                                got_one = True
                                exit  # jump out the loop -- so the first matching timezone has priority over any others
                if not got_one:
                        sys.exit('Fail: time=%5.2f not within any timezone in os=%s' % (time, os))
                return (schedrange, mode)
        

        n_buyers = trader_stats['n_buyers']
        n_sellers = trader_stats['n_sellers']
        n_traders = n_buyers + n_sellers

        shuffle_times = True


        if len(pending) < 1:
                # list of pending (to-be-issued) customer orders is empty, so generate a new one
                new_pending = []

                # demand side (buyers)
                issuetimes = getissuetimes(n_buyers, os['timemode'], os['interval'], shuffle_times, True)
                
                ordertype = 'Bid'
                (sched, mode) = getschedmode(time, os['dem'])             
                for t in range(n_buyers):
                        issuetime = time + issuetimes[t]
                        tname = 'B%02d' % t
                        orderprice = getorderprice(t, sched, n_buyers, mode, issuetime)
                        order = Order(tname, ordertype, orderprice, 1, issuetime)
                        new_pending.append(order)
                        
                # supply side (sellers)
                issuetimes = getissuetimes(n_sellers, os['timemode'], os['interval'], shuffle_times, True)
                ordertype = 'Ask'
                (sched, mode) = getschedmode(time, os['sup'])
                for t in range(n_sellers):
                        issuetime = time + issuetimes[t]
                        tname = 'S%02d' % t
                        orderprice = getorderprice(t, sched, n_sellers, mode, issuetime)
                        order = Order(tname, ordertype, orderprice, 1, issuetime)
                        new_pending.append(order)
        else:
                # there are pending future orders: issue any whose timestamp is in the past
                new_pending = []
                for order in pending:
                        if order.time < time:
                                # this order should have been issued by now
                                # issue it to the trader
                                tname = order.tid
                                traders[tname].add_order(order)
                                if verbose: print('New order: %s' % order)
                                # and then don't add it to new_pending (i.e., delete it)
                        else:
                                # this order stays on the pending list
                                new_pending.append(order)

        return new_pending



# one session in the market
def market_session(sess_id, starttime, endtime, trader_spec, order_schedule, dumpfile, dump_each_trade):


        # initialise the exchange
        exchange = Exchange()


        # create a bunch of traders
        traders = {}
        trader_stats = populate_market(trader_spec, traders, True, False)


        # timestep set so that can process all traders in one second
        # NB minimum interarrival time of customer orders may be much less than this!! 
        timestep = 1.0 / float(trader_stats['n_buyers'] + trader_stats['n_sellers'])
        
        duration = float(endtime - starttime)

        last_update = -1.0

        time = starttime

        orders_verbose = False
        lob_verbose = False
        process_verbose = False
        respond_verbose = False
        bookkeep_verbose = False

        pending_orders = []       

        while time < endtime:

                # how much time left, as a percentage?
                time_left = (endtime - time) / duration

# #                print('%s; t=%08.2f (%4.1f) ' % (sess_id, time, time_left*100))

                trade = None

                pending_orders = customer_orders(time, last_update, traders, trader_stats,
                                                 order_schedule, pending_orders, orders_verbose)


                # get an order (or None) from a randomly chosen trader
                tid = list(traders.keys())[random.randint(0, len(traders) - 1)]
                order = traders[tid].getorder(time, time_left, exchange.publish_lob(time, lob_verbose))

                if order != None:
                        # send order to exchange
                        trade = exchange.process_order2(time, order, process_verbose)
                        if trade != None:
                                # trade occurred,
                                # so the counterparties update order lists and blotters
                                traders[trade['party1']].bookkeep(trade, order, bookkeep_verbose)
                                traders[trade['party2']].bookkeep(trade, order, bookkeep_verbose)
                                if dump_each_trade: trade_stats(sess_id, traders, tdump, time, exchange.publish_lob(time, lob_verbose))

                        # traders respond to whatever happened
                        lob = exchange.publish_lob(time, lob_verbose)
                        for t in traders:
                                # NB respond just updates trader's internal variables
                                # doesn't alter the LOB, so processing each trader in
                                # seqeunce (rather than random/shuffle) isn't a problem
                                traders[t].respond(time, lob, trade, respond_verbose)

                time = time + timestep


        # end of an experiment -- dump the tape
        exchange.tape_dump('transactions.csv', 'w', 'keep')


        # write trade_stats for this experiment NB end-of-session summary only
        trade_stats(sess_id, traders, tdump, time, exchange.publish_lob(time, lob_verbose))



#############################

# # Below here is where we set up and run a series of experiments


if __name__ == "__main__":

        # set up parameters for the session

        start_time = 0.0
        end_time = 600.0
        duration = end_time - start_time


        # schedule_offsetfn returns time-dependent offset on schedule prices
        def schedule_offsetfn(t):
                pi2 = math.pi * 2
                c = math.pi * 3000
                wavelength = t / c
                gradient = 100 * t / (c / pi2)
                amplitude = 100 * t / (c / pi2)
                offset = gradient + amplitude * math.sin(wavelength * t)
                return int(round(offset, 0))
                
                

# #        range1 = (10, 190, schedule_offsetfn)
# #        range2 = (200,300, schedule_offsetfn)

# #        supply_schedule = [ {'from':start_time, 'to':duration/3, 'ranges':[range1], 'stepmode':'fixed'},
# #                            {'from':duration/3, 'to':2*duration/3, 'ranges':[range2], 'stepmode':'fixed'},
# #                            {'from':2*duration/3, 'to':end_time, 'ranges':[range1], 'stepmode':'fixed'}
# #                          ]



        range1 = (95, 95, schedule_offsetfn)
        supply_schedule = [ {'from':start_time, 'to':end_time, 'ranges':[range1], 'stepmode':'fixed'}
                          ]

        range1 = (105, 105, schedule_offsetfn)
        demand_schedule = [ {'from':start_time, 'to':end_time, 'ranges':[range1], 'stepmode':'fixed'}
                          ]

        order_sched = {'sup':supply_schedule, 'dem':demand_schedule,
                       'interval':30, 'timemode':'drip-poisson'}

        buyers_spec = [('AA', 5), ('ZIP', 5), ('SNPR2', 5), ('AS', 5)]
        #sellers_spec = buyers_spec
        sellers_spec = [('AA', 5), ('ZIP', 5), ('SNPR2', 5), ('AS', 5)] 
        traders_spec = {'sellers':sellers_spec, 'buyers':buyers_spec}

        # run a sequence of trials, one session per trial

        n_trials = 100  
        tdump=open('avg_balance.csv','w')
        #tdump = sys.stdout
        trial = 1
        if n_trials > 1:
               dump_all = False
        else:
               dump_all = True
       
        while (trial<(n_trials+1)):
               trial_id = 'trial%04d' % trial
               market_session(trial_id, start_time, end_time, traders_spec, order_sched, tdump, dump_all)
               tdump.flush()
               trial = trial + 1
               print('done', trial-1)
        tdump.close()

        #sys.exit('Done Now')

        

        # run a sequence of trials that exhaustively varies the ratio of four trader types
        # NB this has weakness of symmetric proportions on buyers/sellers -- combinatorics of varying that are quite nasty
        

        # n_trader_types = 4
        # equal_ratio_n = 4
        # n_trials_per_ratio = 50
        # 
        # n_traders = n_trader_types * equal_ratio_n
        # 
        # fname = 'balances_%03d.csv' % equal_ratio_n
        # 
        # tdump = open(fname, 'w')
        # 
        # min_n = 1
        # 
        # trialnumber = 1
        # trdr_1_n = min_n
        # while trdr_1_n <= n_traders:
        #         trdr_2_n = min_n 
        #         while trdr_2_n <= n_traders - trdr_1_n:
        #                 trdr_3_n = min_n
        #                 while trdr_3_n <= n_traders - (trdr_1_n + trdr_2_n):
        #                         trdr_4_n = n_traders - (trdr_1_n + trdr_2_n + trdr_3_n)
        #                         if trdr_4_n >= min_n:
        #                                 buyers_spec = [('GVWY', trdr_1_n), ('SHVR', trdr_2_n),
        #                                                ('ZIC', trdr_3_n), ('ZIP', trdr_4_n)]
        #                                 sellers_spec = buyers_spec
        #                                 traders_spec = {'sellers':sellers_spec, 'buyers':buyers_spec}
        #                                 print buyers_spec
        #                                 trial = 1
        #                                 while trial <= n_trials_per_ratio:
        #                                         trial_id = 'trial%07d' % trialnumber
        #                                         market_session(trial_id, start_time, end_time, traders_spec,
        #                                                        order_sched, tdump, False)
        #                                         tdump.flush()
        #                                         trial = trial + 1
        #                                         trialnumber = trialnumber + 1
        #                         trdr_3_n += 1
        #                 trdr_2_n += 1
        #         trdr_1_n += 1
        # tdump.close()
        # 
        # print trialnumber


